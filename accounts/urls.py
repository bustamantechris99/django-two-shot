from django.urls import path
from .views import LoginUser, LogOut, SignUp

urlpatterns = [
    path('login/', LoginUser, name='login'),
    path('logout/', LogOut, name='logout'),
    path('signup/', SignUp, name='signup')
]
