from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
      'name',
      'owner'
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
      'vendor',
      'date',
      'purchaser',
      'account',
      'category',
      'tax',
      'total'


    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
      'name',
      'owner',
      'number'
    )